#!/bin/bash

###############################################################################
#                      Created by K.Michael                                   #      
###############################################################################

SUBJECT_PRIORIDAD_2="IDS Notificacion de seguridad PRIORIDAD 2 - REVISION del Servidor necesaria"
SUBJECT_PRIORIDAD_1="IDS Notificacion de seguridad PRIORIDAD 1 - URGENTE Revision del Servidor"
EMAILS="YOUR_EMAIL_HERE"
IDS_LOG=$(tail -n 100 /var/log/suricata/fast.log)

while sleep 60; do
        if grep -q "Priority: 1" <<<"$IDS_LOG"; then
        	if [[ -f tempIdsEmail ]]; then
        		rm -rf tempIdsEmail
        		echo "Atencion URGENTE Atencion URGENTE Atencion URGENTE Atencion URGENTE Atencion" >> tempIdsEmail
        		echo " "
        		echo "Comunicarse con el administrador de sistemas, posible BRECHA de SEGURIDAD" >> tempIdsEmail
        		echo "El sistema para la deteccion de instrusos ha detectado un evento PRIORIDAD 1" >> tempIdsEmail 
        		echo " " 
        		echo "Acontinuacion se detallan los eventos asociados segun prioridades: " >> tempIdsEmail
        		echo " " >> tempIdsEmail
            else
            	echo "Atencion Atencion Atencion Atencion Atencion Atencion Atencion Atencion Atencion" >> tempIdsEmail
        		echo " "
        		echo "Comunicarse con el administrador de sistemas, posible BRECHA de SEGURIDAD" >> tempIdsEmail
        		echo "El sistema para la deteccion de instrusos ha detectado un evento PRIORIDAD 1" >> tempIdsEmail 
        		echo " " 
        		echo "Acontinuacion se detallan los eventos asociados segun prioridades: " >> tempIdsEmail
        		echo " " >> tempIdsEmail	
        	fi
            tail -n 50 /var/log/suricata/fast.log | grep "Priority: 1" >> tempIdsEmail && cat tempIdsEmail | mail -s "${SUBJECT_PRIORIDAD_1}" ${EMAILS}
                
        elif grep -q "Priority: 3" <<<"$IDS_LOG"; then
        	
        	if [[ -f tempIdsEmail ]]; then
        		rm -rf tempIdsEmail
        		echo "Atencion Atencion Atencion Atencion Atencion Atencion Atencion Atencion Atencion" >> tempIdsEmail
        		echo " "
        		echo "Comunicarse con el administrador de sistemas para Revision del servidor y establecer la peligrosidad del evento" >> tempIdsEmail
        		echo "El sistema para la deteccion de instrusos ha detectado un evento PRIORIDAD 2" >> tempIdsEmail 
        		echo " " 
        		echo "Acontinuacion se detallan los eventos asociados segun prioridades: " >> tempIdsEmail
        		echo " " >> tempIdsEmail
            else
            	echo "Atencion Atencion Atencion Atencion Atencion Atencion Atencion Atencion Atencion" >> tempIdsEmail
        		echo " "
        		echo "Comunicarse con el administrador de sistemas para Revision del servidor y establecer la peligrosidad del evento" >> tempIdsEmail
        		echo "El sistema para la deteccion de instrusos ha detectado un evento PRIORIDAD 2" >> tempIdsEmail 
        		echo " " 
        		echo "Acontinuacion se detallan los eventos asociados segun prioridades: " >> tempIdsEmail
        		echo " " >> tempIdsEmail	
        	fi

        	tail -n 50 /var/log/suricata/fast.log | grep "Priority: 3" >> tempIdsEmail && cat tempIdsEmail | mail -s "${SUBJECT_PRIORIDAD_2}" ${EMAILS}

        fi


done
